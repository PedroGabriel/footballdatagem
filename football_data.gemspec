# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'football_data/version'

Gem::Specification.new do |spec|
  spec.name          = "football_data"
  spec.version       = FootballData::VERSION
  spec.authors       = ["PedroGabriel"]
  spec.email         = ["pedrogabriel@protonmail.ch"]

  spec.summary       = %q{Quick access to the Football-Data API}
  spec.description   = %q{This gem intends to provide easy access to the REST API provided by Football-Data}
  spec.homepage      = "https://bitbucket.org/PedroGabriel/footballdatagem"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.10"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "json", '~> 1.8', '>= 1.8.3'
  spec.add_development_dependency "rspec", '~> 3.3'
  spec.add_development_dependency "rest-client", '~> 1.8'
end
