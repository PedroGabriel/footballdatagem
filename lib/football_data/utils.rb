module Utils
    def self.request(uri)
      begin
        api_key =  FootballData.class_variable_get(:@@api_key)
        response = RestClient.get uri,  {'x-auth-token' => api_key}
        return JSON.parse(response.body) if response.code == 200
        nil
      rescue
        return nil
      end
    end
  end
