require_relative './link'

class Team
  attr_reader :name, :market_value, :code, :short_name, :crest_url, :links, :id

  def initialize(team_hash)
    @name = team_hash['name']
    @code = team_hash['code']
    @short_name = team_hash['shortName']
    @crest_url = team_hash['crestUrl']
    @market_value = team_hash['squadMarketValue']
    @links = Link.new(team_hash['_links'])
    @id = @links.self.split('/').pop
  end


  def to_s
    rep = "#{@name}"
    rep = " - (#{@code}) " unless @code.nil? 
    rep + " - #{@short_name}"
  end
end
