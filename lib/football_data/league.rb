require_relative '../football_data/link'

require_relative '../football_data'
require_relative './utils'
extend Utils

require_relative './team'
require_relative './standing'
require 'rest-client'

##
# DTO Class that represents the concept of a league.
#
class League
  attr_reader :caption, :league, :year, :number_of_teams, :number_of_games, 
              :links, :id
  

  def initialize(season_hash)
    raise ArgumentError.new('JSON object is invalid') if season_hash.nil?

    @caption = season_hash['caption'] || nil #TODO rename
    @league = season_hash['league'] || nil  #TODO rename
    @year = season_hash['year'] || nil
    @number_of_teams = season_hash['numberOfTeams'] || nil
    @number_of_games = season_hash['numberOfGames'] || nil
    @links = Link.new(season_hash['_links'])
    @id = @links.self.split('/').pop
  
    @teams = []
  end

  ##
  # Returns an array of Team that represents all the teams inside a certain
  # season. It uses an internal "buffer" that gets returned if any other call
  # to this method was made previously. This may happen since for an entire 
  # season the set of teams is constant.
  #
  def get_teams
    return @teams unless @teams.empty?
    season_hash_teams = Utils.request(@links.teams) 
    season_hash_teams['teams'].each do |team|
      @teams.push Team.new(team)
    end
    @teams
  end

  ##
  # 
  #
  def get_team(name)
    get_teams.each { |team| return team if team.name.eql?(name) or team.short_name.eql?(name) }
    nil
  end

  ##
  # Printable version of League instance. 
  #
  def to_s
    "(#{@id})#{@league} - #{@caption} - #{@year}"
  end

  ##
  # Verifies if the target team has the same name as
  # the parameter. You can pass the name or the short name.
  #
  def has_eql_name(name)
    @caption.eql? name or @league.eql? name
  end

  ##
  # Verifies if the target team has the same id
  # as the id passed as parameter.
  #
  def has_eql_id(id)
    @id.eql? id
  end

  ##
  # Returns an array with instances of Fixture. It does not use an internal 
  # array since the API does not hold all the fixtures since the beginning of the
  # season. It keep adding as time goes by which makes every call return a new
  # value every time. 
  #
  def all_fixtures
    fixtures = []
    league_hash_fixtures = Utils.request(@links.fixtures)
    league_hash_fixtures['fixtures'].each do |fix|
      fixtures.push Fixture.new(fix)
    end
    fixtures
  end

  ##
  # It tries to return a given matchday. Instead of returning all the fixtures,
  # it filters and returns only one if the parameter equals to the id present
  # in any of the matchdays.
  def get_matchday(matchday_number)
    throw ArgumentError if matchday_number < 1
    fixtures_from_matchday = []
    self.all_fixtures.each do |matchday| 
      fixtures_from_matchday.push matchday if matchday.match_day == matchday_number
    end
    fixtures_from_matchday
  end

  ##
  # Tries to acquire the resource given by the the URI "leaguetable"
  # present in the main resource (all seasons).
  # It does not use a buffer mechanism since after every week all the
  # league tables might change according to the results of each fixture.
  def get_leaguetable
    leaguetable = []
    league_hash_leaguetable = Utils.request(@links.league_table)
    league_hash_leaguetable['standing'].each do |standing_hash| 
      leaguetable.push Standing.new(standing_hash)
    end
    leaguetable
  end
end
