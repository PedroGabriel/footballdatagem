
class Link

  attr_reader :self, :teams, :fixtures, :league_table
  attr_reader :team

  def initialize(link_hash)
    raise ArgumentError.new('JSON object is invalid') if link_hash.nil?

    @self = link_hash['self']['href'] unless link_hash['self'].nil?
    @team = link_hash['team']['href'] unless link_hash['team'].nil?
    @teams = link_hash['teams']['href'] unless link_hash['teams'].nil?
    @fixtures = link_hash['fixtures']['href'] unless link_hash['fixtures'].nil?
    @league_table = link_hash['leagueTable']['href'] unless link_hash['leagueTable'].nil?
  end

end
