
class Standing

  attr_reader :links, :position, :team_name, :points, :played_games
  attr_reader :goals, :goals_against, :goal_difference, :wins, :draws, :losses

  def initialize(standing_hash)
    @links = Link.new(standing_hash['_links'])
    @position = standing_hash['position']
    @team_name = standing_hash['teamName']
    @played_games = standing_hash['playedGames']
    @points = standing_hash['points']
    @goals = standing_hash['goals']
    @goals_against = standing_hash['goalsAgainst']
    @goal_difference = standing_hash['goalDifference']
    @wins = standing_hash['wins']
    @draws = standing_hash['draws']
    @losses = standing_hash['losses']
  end

  def to_s
    "#{@position} - #{@team_name} - #{@points} Points"
  end

end
