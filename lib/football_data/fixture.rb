require_relative './link'
class Fixture

  attr_reader :date, :status, :match_day, :home_team_name, :away_team_name,
              :result, :links, :id

  def initialize(fixture_hash)
    @date = fixture_hash['date']
    @status = fixture_hash['status']
    @match_day = fixture_hash['matchday']
    @home_team_name = fixture_hash['homeTeamName']
    @away_team_name = fixture_hash['awayTeamName']
    @result = fixture_hash['result'] #gives an hash as well
    @links = Link.new(fixture_hash['_links'])
    @id = @links.self.split('/').pop
  end

  ##
  # Gives back the amount of goals related
  # to the home team.
  #
  def goals_home_team
    @result['goalsHomeTeam']
  end

  ##
  # Gives back the amount of goals related
  # to the away team.
  #
  def goals_away_team
    @result['goalsAwayTeam']
  end

  ##
  # Printable version of Fixture instance.
  #
  def to_s
    "(#{@id}) - Match Day #{@match_day} #{@home_team_name} vs #{@away_team_name} - #{@status}"
  end

end
