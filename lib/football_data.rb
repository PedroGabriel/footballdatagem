
require "json"
require "rest-client"

module FootballData

  private

  @@api_key

  public

  def self.set_api_key(api_key)
    @@api_key = api_key
  end

  ##
  # 
  #
  def self.all_leagues
    uri = 'http://api.football-data.org/v1/soccerseasons/'
    leagues = []
    begin
      json = Utils::request uri
      json.each do |league|
        leagues.push League.new(league)
      end
      return leagues
    rescue
      return leagues
    end
  end

  ##
  # Each season is based upon a certain league.
  # In order to select a certain season we must provide the league's
  # name in order to select the most recent season.
  #
  # @param name_or_id League's name
  #
  def self.get_league(name_or_id)
    return nil if name_or_id.nil?
    leagues = self.all_leagues
    leagues.each do |league|
      return league if league.has_eql_name(name_or_id) or league.has_eql_id(name_or_id)
    end
    nil
  end
end
