require 'spec_helper'

describe "football_data behavior" do
  before(:all) do
    FootballData.set_api_key 'c4455c6ed8314e59af4377f6063d2c53'
  end

  context 'when trying to obtain all leagues' do
    it 'must return an array with League instances' do
      all_leagues_from_api = FootballData.all_leagues
      expect(all_leagues_from_api.empty?).to be_falsey
      expect(all_leagues_from_api.count).to be_eql 12
    end
  end

  context 'when trying to obtain a certain league' do
    it 'must be possible to obtain a certain league according to the name specified' do
      SEASON_NAME = "1. Bundesliga 2015/16"
      season = FootballData.get_league SEASON_NAME
      expect(season).to be_a_kind_of League
      expect(season.caption).to eql SEASON_NAME
    end

    it 'must work as well when we supply the short name of a certain league' do
      SHORT_NAME = "BL1";
      s = FootballData.get_league SHORT_NAME
      expect(s).to be_a_kind_of League
      expect(s.league).to eql SHORT_NAME
      expect(s.id).to eql "394"
    end

    it 'must work as well when we supply the id of a certain league' do
      ID = "394"
      s = FootballData.get_league ID
      expect(s).to be_a_kind_of League
      expect(s.league).to eql "BL1"
      expect(s.id).to eql ID
    end

    it 'must return nil when we supply an unavailable short name' do
      _short_name = "dummy_name";
      s = FootballData.get_league _short_name
      expect(s).to be_nil
    end
  end

  

end
