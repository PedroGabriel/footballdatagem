require 'spec_helper'

describe 'league behavior' do

  before(:all) do
    FootballData.set_api_key 'c4455c6ed8314e59af4377f6063d2c53'
  end

  context 'when object data is correctly supplied' do
    it 'must return an object with the following properties' do
      mock = JSON.parse(%Q({"_links":{"self":{"href":"http://api.football-data.org/v1/soccerseasons/394"},"teams":{"href":"http://api.football-data.org/v1/soccerseasons/394/teams"},"fixtures":{"href":"http://api.football-data.org/v1/soccerseasons/394/fixtures"},"leagueTable":{"href":"http://api.football-data.org/v1/soccerseasons/394/leagueTable"}},"caption":"1. Bundesliga 2015/16","league":"BL1","year":"2015","numberOfTeams":18,"numberOfGames":306,"lastUpdated":"2015-11-01T19:42:35Z"}))
      league = League.new(mock)
      expect(league).to have_attributes(:year => "2015", 
        :caption => "1. Bundesliga 2015/16",
        :league => "BL1", 
        :number_of_teams => 18, 
        :number_of_games => 306)
      expect(league.links).to have_attributes(:self => "http://api.football-data.org/v1/soccerseasons/394",
                                              :teams => "http://api.football-data.org/v1/soccerseasons/394/teams",
                                              :fixtures => "http://api.football-data.org/v1/soccerseasons/394/fixtures",
                                              :league_table => "http://api.football-data.org/v1/soccerseasons/394/leagueTable")
    end
  end

  context 'when mock object is incorrectly supplied' do
    it 'must throw ArgumentException' do
      begin
        League.new(nil)
      rescue Exception => e
        expect(e).to be_a_kind_of ArgumentError
        expect(e.message).to eql 'JSON object is invalid'
      end
    end
  end

  context 'when trying to get all the teams related to a certain league' do
    it 'must return an array with Team objects representing each team' do
      league = FootballData.all_leagues[0]
      all_teams = league.get_teams
      expect(all_teams.size).to eql 18
    end

    it 'must be possible to obtain just one specific team' do
      leagues = FootballData.all_leagues[0] #german league
      team = leagues.get_team("HSV")        #short name or full
      expect(team.name).to eql "Hamburger SV"
      expect(team.code).to eql "HSV"        # might be nil sometimes
      expect(team.short_name).to eql "HSV"   #shortName and code do not depend on each other
    end

    it 'must return nil when there isn\'t a team with that name '  do
      leagues = FootballData.all_leagues[0]
      team = leagues.get_team('unknown')
      expect(team).to be_nil
    end
  end

  context 'when trying to get all the fixtures related to a certain league' do
    it 'must be possible to obtain an array with all fixtures' do
      premier_league = FootballData.get_league 'PL'
      fixtures = premier_league.all_fixtures
      expect(fixtures).to be_a_kind_of Array
      expect(fixtures[0]).to be_a_kind_of Fixture
    end

    it 'must be possible to obtain a specific fixture related to a certain league' do
      premier_league = FootballData.get_league 'PL'
      matchday_two = premier_league.get_matchday 2
      expect(matchday_two).to be_a_kind_of Array
      expect(matchday_two[0]).to be_a_kind_of Fixture
    end

    it 'must throw an expection when trying to get an invalid fixture' do
      premier_league = FootballData.get_league 'PL'
      begin
        premier_league.get_matchday(-1)
      rescue => invalid_fixture
        expect(invalid_fixture).to be_a_kind_of ArgumentError
      end
    end
  end

  context 'when trying to obtain the leaguetable for a certain league' do
    it 'must return an array of Standing instances if everything goes correctly' do
      premier_league = FootballData.get_league 'PL'
      standings = premier_league.get_leaguetable
      expect(standings).to be_a_kind_of Array
      expect(standings[0]).to be_a_kind_of Standing 
    end

    
  end

end
