# FootballData

This gem intends to provide easy access to the REST API provided by [Football-Data](http://api.football-data.org)
I suggest you experiment with an api key of your own.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'football_data'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install football_data

## Usage

TBA
